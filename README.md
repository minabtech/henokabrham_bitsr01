This is personal portfolio that contain for "My Academic Profile" page for minabTech Assignment. It includes a header, navigation bar, and four sections: basic details, bio, education details, and contact. The code uses CSS for styling and JavaScript for form validation.

The "basic-detail" section displays a profile picture and some basic information about the person, including their name, address, date and place of birth, and career.

The "bio" section provides a brief biography of the person, including their passion for web development, their experience with programming languages and web development technologies, and their goal of creating innovative and user-friendly web applications.
The "education" section lists the person's education details, including their higher education, high school, and primary school. Each education level includes the name of the institution, the degree or diploma received, and the graduation year.
The "contact" section includes a contact form that allows visitors to send an email to the person. The form includes input fields for the visitor's name, email, and message, and a submit button. The JavaScript code adds form validation to ensure that all required fields are filled out before submitting the form.
